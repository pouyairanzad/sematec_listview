package com.sematec.customlistviewcity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TextView txtrank;
        TextView  txtcountry;
        TextView  txtpopulation;
        ImageView imgflag;
        String    rank;
        String    country;
        String    population;
        int       flag = 0;


        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_layout);
        txtrank =  findViewById(R.id.rank);
        txtcountry =  findViewById(R.id.country);
        txtpopulation =  findViewById(R.id.population);


        imgflag =  findViewById(R.id.flag);
        Intent i = getIntent();

        rank = i.getStringExtra("rank");
        country = i.getStringExtra("country");
        population = i.getStringExtra("population");
        flag = i.getIntExtra("flag", flag);

        txtrank.setText(rank);
        txtcountry.setText(country);
        txtpopulation.setText(population);
        imgflag.setImageResource(flag);
    }

}


