package com.sematec.customlistviewcity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


    public class ListViewAdapter extends BaseAdapter {


        Context                            mContext;
        LayoutInflater                     inflater;
        private List<WorldPopulation>      worldpopulationlist = null;
        private ArrayList<WorldPopulation> arraylist;


        public ListViewAdapter(Context context,
                               List<WorldPopulation> worldpopulationlist) {
            mContext = context;
            this.worldpopulationlist = worldpopulationlist;
            inflater = LayoutInflater.from(mContext);
            this.arraylist = new ArrayList<WorldPopulation>();
            this.arraylist.addAll(worldpopulationlist);
        }


        public class ViewHolder {

            TextView  rank;
            TextView  country;
            TextView  population;
            ImageView flag;
        }


        @Override
        public int getCount() {
            return worldpopulationlist.size();
        }


        @Override
        public WorldPopulation getItem(int position) {
            return worldpopulationlist.get(position);
        }


        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            final ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.row_layout, null);

                holder.rank =  view.findViewById(R.id.rank);
                holder.country =  view.findViewById(R.id.country);
                holder.population =  view.findViewById(R.id.population);

                holder.flag =  view.findViewById(R.id.flag);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.rank.setText(worldpopulationlist.get(position).getRank());
            holder.country.setText(worldpopulationlist.get(position).getCountry());
            holder.population.setText(worldpopulationlist.get(position)
                    .getPopulation());

            holder.flag.setImageResource(worldpopulationlist.get(position)
                    .getFlag());

            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {

                    Intent intent = new Intent(mContext, DetailsActivity.class);

                    intent.putExtra("rank",
                            (worldpopulationlist.get(position).getRank()));

                    intent.putExtra("country",
                            (worldpopulationlist.get(position).getCountry()));

                    intent.putExtra("population",
                            (worldpopulationlist.get(position).getPopulation()));

                    intent.putExtra("flag",
                            (worldpopulationlist.get(position).getFlag()));

                    mContext.startActivity(intent);
                }
            });

            return view;
        }



        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            worldpopulationlist.clear();
            if (charText.length() == 0) {
                worldpopulationlist.addAll(arraylist);
            } else {
                for (WorldPopulation wp: arraylist) {
                    if (wp.getCountry().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        worldpopulationlist.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }

    }


